﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Journal
    {
        public List<Records> jornalRecord = new List<Records>();

        public void AddJornalRecord(List<Book> books)
        {
            Console.WriteLine("Enter your name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter book name: ");
            string bookName = Console.ReadLine();
            Console.WriteLine("Enter book author: ");
            string bookAuthor = Console.ReadLine();
            Console.WriteLine("Enter the number of days you will return this book:");
            int days = Convert.ToInt32(Console.ReadLine());
            Records records = new Records(name, bookName, bookAuthor, days);

            if (!books.Exists(x => x.Name == bookName) || !books.Exists(x => x.Author == bookAuthor))
            {
                Console.WriteLine("This book dont exists");
            }
            else
            {
                jornalRecord.Add(records);
            }
        }

	public void ShowRecords()
        {
            foreach (var element in jornalRecord)
            {
                Console.WriteLine("\nName: " + element.Name + "\n" + element.BookName + " - " + element.BookAuthor  + "\nDays after which the book will be returned: " + element.Days);
            }
        }

	public void ShowStatistics(List<Book> books, List<Records> logEntries)
        {
            int numberOfDays = 0;
            foreach (var element in logEntries)
            {
                numberOfDays += element.Days;
                Console.WriteLine($"Title: " + element.BookName + "\nAuthor: " + element.BookAuthor);
            }
            Console.WriteLine("Total number of days that readers had a book: " + numberOfDays + "\n");
        }
    }
}
