﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Records
    {
        public string Name;
        public string BookName;
        public string BookAuthor;
        public int Days;

        public Records(string name, string bookName, string bookAuthor, int days)
        {
            Name = name;
            BookName = bookName;
            BookAuthor = bookAuthor;
            Days = days;
        }
    }
}
