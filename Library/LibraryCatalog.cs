﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class LibraryCatalog
    {
        public List<Book> booksInLibrary = new List<Book>();
        Book firstBook = new Book("Hugh Lofting", "The Story of Doctor Dolittle");
        Book secondBook = new Book("Mary Shelley", "Frankenstein");
        Book thirdBook = new Book("Bram Stoker", "Dracula");
        Book fourthBook = new Book("Anna Sewell", "Black Beauty");
        Book fivesBook = new Book("Emily Bronte", "Wuthering Heights");

        public LibraryCatalog()
        {
            booksInLibrary.Add(firstBook);
            booksInLibrary.Add(secondBook);
            booksInLibrary.Add(thirdBook);
            booksInLibrary.Add(fourthBook);
            booksInLibrary.Add(fivesBook);
        }

        public void ShowLibraryCatalog()
        {
            foreach (var element in booksInLibrary)
            {
                Console.WriteLine(element.Author + " - " + element.Name);
            }
            Console.WriteLine();
        }

        public void ChangeTheCatalog()
        {
            char choice = Menu.ChangeCatalog();
            switch (choice)
            {
                case '1':
                    AddBookToCatalog();
                    break;
                case '2':
                    DeleteBookFromCatalog();
                    break;
                case '3':
                    SortCatalog();
                    break;
                case '4':
                    SearchOfCatalog();
                    break;
            }
        }

        public void AddBookToCatalog()
        {
            Console.Write("\nEnter author of book: ");
            string author = Console.ReadLine();
            Console.Write("Enter name of book: ");
            string name = Console.ReadLine();
            Book newBook = new Book(author, name);
            booksInLibrary.Add(newBook);
        }

        public void DeleteBookFromCatalog()
        {
            Console.Write("Enter the name of book you want to delete: ");
            string name = Console.ReadLine();
            var removeBook = new List<string>() { name };
            booksInLibrary.RemoveAll(x => removeBook.Any(y => y == x.Name));
        }

        public void SortCatalog()
        {
            Console.WriteLine("1 - Sort book by author\n2 - Sort book by book's name\n3 - Show all books by one author\n");
            char action = Convert.ToChar(Console.ReadLine());

            switch (action)
            {
                case '1':
                    var sortBooksByAuthor = booksInLibrary.OrderBy(item => item.Author).ToList();
                    foreach (var element in sortBooksByAuthor)
                    {
                        Console.WriteLine(element.Author + " - " + element.Name);
                    }
                    Console.WriteLine();
                    break;
                case '2':
                    var sortBooksByName = booksInLibrary.OrderBy(item => item.Name).ToList();
                    foreach (var element in sortBooksByName)
                    {
                        Console.WriteLine(element.Name + " - " + element.Author);
                    }
                    Console.WriteLine();
                    break;
                case '3':
                    Console.Write("Enter the name of author: ");
                    string nameAuthor = Console.ReadLine();

                    if (!booksInLibrary.Exists(x => x.Author == nameAuthor))
                    {
                        Console.WriteLine("This author doesn't exist.");
                    }
                    else
                    {
                        foreach (var element in booksInLibrary)
                        {
                            if (element.Author == nameAuthor)
                            {
                                Console.WriteLine(element.Author + " - " + element.Name);
                            }
                        }
                    }
                    break;

            }
        }

        public void SearchOfCatalog()
        {
            Console.Write("Search: ");
            string temp = Console.ReadLine();

            foreach (Book element in booksInLibrary)
            {
                if (element.Name.Contains(temp))
                {
                    Console.WriteLine(element.Author + " - " + element.Name);
                }
            }
        }
    }
}
