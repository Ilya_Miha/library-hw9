﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public static class Menu
    {
        public static char ChooseActionInMenu()
        {
            Console.WriteLine("1 - Check catalog of the library\n2 - Actions in catalog (Add/Remove/Sort/Search)\n3 - Take the book\n4 - Check the journal\n5 - Show statistics\n6 - Exit");
            char action = Convert.ToChar(Console.ReadLine());
            return action;
        }
        
        public static char ChangeCatalog()
        {
            Console.WriteLine("1 - Add new book\n2 - Remove the book\n3 - Sort the books\n4 - Search the book");
            char action = Convert.ToChar(Console.ReadLine());
            return action;
        }
        
        
    }
}
