﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Book
    {
       public string Author;
        public string Name;

        public Book(string author, string name)
        {
            Author = author;
            Name = name;
        }
    }
}
