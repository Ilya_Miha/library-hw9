﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class MainClass
    {
        static void Main(string[] args)
        {
            LibraryCatalog libraryCatalog = new LibraryCatalog();
            Journal journal = new Journal();

            do
            {
                char selection = Menu.ChooseActionInMenu();
                Console.Clear();
                switch (selection)
                {
                    case '1':
                        libraryCatalog.ShowLibraryCatalog();
                        Console.WriteLine("Press any key to continue!");
                        Console.ReadLine();
                        break;
                    case '2':
                        libraryCatalog.ChangeTheCatalog();
                        Console.WriteLine("Press any key to continue!");
                        Console.ReadLine();
                        break;
                    case '3':
                        journal.AddJornalRecord(libraryCatalog.booksInLibrary);
                        Console.WriteLine("Press any key to continue!");
                        Console.ReadLine();
                        break;
                    case '4':
                        journal.ShowRecords();
                        Console.WriteLine("Press any key to continue!");
                        Console.ReadLine();
                        break;
                    case '5':
                        journal.ShowStatistics(libraryCatalog.booksInLibrary, journal.jornalRecord);
                        break;
                    case '6':
                        return;
                }
            } while (true);
        }
    }
}
